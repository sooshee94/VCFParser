package com.company.utils;

import com.opencsv.CSVWriter;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;

public class CSVUtils {
    private static CSVUtils instance;

    private CSVUtils() {}

    public static CSVUtils getInstance() {
        if (instance == null)
            instance = new CSVUtils();
        return instance;
    }
    public CSVWriter getCSVWriter(String path) throws IOException {
        Writer writer = Files.newBufferedWriter(Paths.get(path));

        return new CSVWriter(writer,
                CSVWriter.DEFAULT_SEPARATOR,
                CSVWriter.NO_QUOTE_CHARACTER,
                CSVWriter.DEFAULT_ESCAPE_CHARACTER,
                CSVWriter.DEFAULT_LINE_END);
    }
}
