package com.company;

import com.company.models.DataLine;
import com.company.utils.CSVUtils;
import com.opencsv.CSVWriter;

import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Stuff {
	private final static Logger LOGGER = Logger.getLogger(Stuff.class.getName());
	private static VCFParser parser = new VCFParser();

	Stuff() {
		parser = new VCFParser();
	}

	void writeCSVFile() {
		LOGGER.log(Level.INFO, "Writing CSV file.");
		try (CSVWriter writer = CSVUtils.getInstance().getCSVWriter("./output_csv.csv")) {
			String[] headers = {"Chr", "Start", "End", "Ref", "Alt", "Gene","AF_ALL","AFR_AF","AMR_AF","EAS_AF","EUR_AF","SAS_AF", "CLNSIG", "CLNDISDB"};
			writer.writeNext(headers);
			List<DataLine> dataLineList = parser.getDataLines();
			for (DataLine dataLine : dataLineList)
				writer.writeAll(dataLine.getOutput());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	void parseVCF(String filePath) {
		try {
			parser.parseFile(filePath);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
