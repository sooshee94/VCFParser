package com.company;

import com.company.models.DataLine;
import com.company.models.Info;
import com.company.models.KeyValueObject;
import htsjdk.samtools.util.BlockCompressedInputStream;
import htsjdk.tribble.index.Block;
import htsjdk.tribble.index.tabix.TabixIndex;
import org.apache.commons.lang3.StringUtils;

import java.io.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class VCFParser {
    private final static Logger LOGGER = Logger.getLogger(VCFParser.class.getName());
    private Map<String, Info> infoMap;
    private List<DataLine> dataLines;
	private TabixIndex dbSNPIndex;
	private Map<String, TabixIndex> thousandGenomeIndices;
	private TabixIndex exACIndex;
	private List<String> dbSNPHeaders;
	private TabixIndex clinVarIndex;

    public Map<String, Info> getInfoMap() {
        return infoMap;
    }

    List<DataLine> getDataLines() {
        return dataLines;
    }

    VCFParser() {
        infoMap = new HashMap<>();
        dataLines = Collections.synchronizedList(new ArrayList<>());
	    try {
		    dbSNPIndex = new TabixIndex(new File("E:/proiect_bdub/00-common_all.vcf.gz.tbi"));
		    exACIndex = new TabixIndex(new File("E:/proiect_bdub/ExAC.r1.sites.vep.vcf.gz.tbi"));
		    clinVarIndex = new TabixIndex(new File("E:/proiect_bdub/clinvar_20180128.vcf.gz.tbi"));
	    } catch (IOException e) {
		    e.printStackTrace();
	    }
	    String[] headers = {"CHROM", "POS", "ID", "REF", "ALT", "QUAL", "FILTER", "INFO"};
	    dbSNPHeaders = Arrays.asList(headers);
	    thousandGenomeIndices = new HashMap<>();
	    String[] keys = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "X", "Y"};
	    List<String> keysList = Arrays.asList(keys);
	    for (String key : keysList) {
		    try {
			    thousandGenomeIndices.put(key, new TabixIndex(new File("E:\\proiect_bdub\\1000G\\ALL.chr" + key + ".phase3_shapeit2_mvncall_integrated_v5a.20130502.genotypes.vcf.gz.tbi")));
		    } catch (IOException e) {
			    e.printStackTrace();
		    }
	    }
    }
    void parseFile(String filePath) throws IOException {
        String line = "";
        List<String> headers = new ArrayList<>();

        BufferedReader br = new BufferedReader(new FileReader(filePath));
        while ((line = br.readLine()) != null) {
            if (line.startsWith("##INFO") || line.startsWith("##FORMAT")) {
                String[] info  = line.split("<(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)", -1)[1].split(">(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)", -1)[0].split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)", -1);
                Info infoObj = new Info();
	            for (String anInfo : info) {
		            KeyValueObject obj = new KeyValueObject(anInfo, '=');
		            infoObj.setField(obj.getKey(), obj.getValue());
		            infoMap.put(infoObj.getID(), infoObj);
	            }
            }
            else if (line.startsWith("##fileformat")) {
                //TODO
            }
            else if (line.startsWith("##FILTER")) {
                //TODO
            }
            else if (line.startsWith("##annotator")) {
                //TODO
            }
            else if (line.startsWith("##UnifiedGenotyper")) {
                //TODO
            }
            else if (line.startsWith("##contig")) {
                //TODO
            }
            else if (line.startsWith("##reference")) {
                //TODO
            }
            else if (line.startsWith("##source")) {
                //TODO
            }
            else if (line.startsWith("#CHROM")) {
                headers = Arrays.asList(line.split("#")[1].split("\t"));
            }
            else if (line.startsWith("##fileDate")) {

            }
            else if (line.startsWith("##dbSNP_BUILD_ID")){

            }
            else if (line.startsWith("##phasing")) {

            }
            else if (line.startsWith("##variationPropertyDocumentationUrl")) {

            }
            else if (line.startsWith("##")) {

            }
            else {
	            DataLine dataLine = getDataLine(line, headers);
	            addGeneInfoToDataLine(dataLine);
	            addThousandGenomeInfoToDataLine(dataLine);
				addClinVarInfoToDataLine(dataLine);
                dataLines.add(dataLine);
                LOGGER.log(Level.INFO, "Adding line " + dataLine.toString());
            }
        }
    }

    private void addClinVarInfoToDataLine(DataLine dataLine) {
    	try {
    		TabixIndex index = clinVarIndex;
    		List<Block> blocks = index.getBlocks(dataLine.getChromosomeNumber(), (int)dataLine.getPosition(), (int)dataLine.getPosition());
    		for (Block block : blocks) {
    			DataLine output = readFileStartingFromPos("E:/proiect_bdub/clinvar_20180128.vcf.gz", block.getStartPosition(), null, dataLine.getID());
    			LOGGER.log(Level.INFO, output.toString());
    			if (output != null) {
    				List<KeyValueObject> infoList = output.getInfo();
    				for (KeyValueObject object : infoList) {
    					switch (object.getKey()) {
							case "CLNSIG":
								dataLine.setCLNSIG(object.getValue());
								break;
							case "CLNDISDB":
								dataLine.setCLNDISDB(object.getValue());
								break;
						}
					}
				}
			}
		}
		catch (Exception e) {
    		e.printStackTrace();
		}
	}

	private void addThousandGenomeInfoToDataLine(DataLine dataLine) {
		try {
			TabixIndex index = thousandGenomeIndices.get(dataLine.getChromosomeNumber());
			List<Block> blocks = index.getBlocks(dataLine.getChromosomeNumber(), (int) dataLine.getPosition(), (int) dataLine.getPosition());
			for (Block block : blocks) {
				LOGGER.log(Level.INFO, "Looking for " + dataLine.toString() + "in 1000g starting from position " + block.getStartPosition());
				DataLine output = readFileStartingFromPos("E:\\proiect_bdub\\1000G\\ALL.chr" + dataLine.getChromosomeNumber() + ".phase3_shapeit2_mvncall_integrated_v5a.20130502.genotypes.vcf.gz", block.getStartPosition(), dataLine.getID(), null);
				if (output != null) {
					LOGGER.log(Level.INFO, output.toString());
					for (KeyValueObject obj : output.getInfo()) {
						switch (obj.getKey()) {
							case "AF":
								dataLine.setAF(obj.getValue());
								break;
							case "AFR_AF":
								dataLine.setAFR_AF(obj.getValue());
								break;
							case "AMR_AF":
								dataLine.setAMR_AF(obj.getValue());
								break;
							case "EAS_AF":
								dataLine.setEAS_AF(obj.getValue());
								break;
							case "EUR_AF":
								dataLine.setEUR_AF(obj.getValue());
								break;
							case "SAS_AF":
								dataLine.setSAS_AF(obj.getValue());
								break;
						}
					}
				}
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void addGeneInfoToDataLine(DataLine dataLine) {
		List<Block> blocks = dbSNPIndex.getBlocks(dataLine.getChromosomeNumber(), (int)dataLine.getPosition(), (int)dataLine.getPosition());
		for (Block block : blocks) {
			LOGGER.log(Level.INFO, "Looking for " + dataLine.toString() + "in dbSNP starting from position " + block.getStartPosition());
			if (!".".equals(dataLine.getID())){
				DataLine output = readFileStartingFromPos("E:/proiect_bdub/00-common_all.vcf.gz", block.getStartPosition(), dataLine.getID(), null);
				if (output != null) {
					String geneInfo = output.getGeneInfo();
					if (geneInfo != null) {
						dataLine.setGene(output.getGeneInfo());
					}
				}
			}
		}
	}

	private DataLine getDataLine(String line, List<String> headers) {
		String[] split = line.split("\t");
		int length = split.length;
		DataLine dataLine = new DataLine();
		for (int i=0;i<headers.size();i++) {
		    dataLine.setField(headers.get(i), split[i]);
		}
		return dataLine;
	}

	private DataLine readFileStartingFromPos(String filePath, long startPosition, String expectedId, String expectedRSID) {
    	int linesParsed = 0;
    	DataLine result = null;
    	String line = "";
	    try {
	    	BlockCompressedInputStream blockCompressedInputStream = new BlockCompressedInputStream(new File(filePath));
	    	blockCompressedInputStream.seek(startPosition);
		    BufferedReader reader = new BufferedReader(new InputStreamReader(blockCompressedInputStream));
		    while ((line = reader.readLine()) != null && linesParsed <= 2000) {
			    linesParsed++;
			    if (expectedId != null) {
					if (!line.contains(expectedId))
						continue;
				}
				DataLine dataLine = getDataLine(line, dbSNPHeaders);
				if (!StringUtils.isEmpty(expectedId)) {
					if (expectedId.equals(dataLine.getID())) {
						LOGGER.log(Level.INFO, "Found it");
						result = dataLine;
						return result;
					}
				}
				else if (!StringUtils.isEmpty(expectedRSID)) {
					if (expectedRSID.equals(dataLine.getRSID())) {
						LOGGER.log(Level.INFO, "Found RSID");
						result = dataLine;
						return result;
					}
				}
		    }
	    }
	    catch (Exception e) {
	    	e.printStackTrace();
	    }
	    return result;
    }
}
