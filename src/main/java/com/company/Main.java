package com.company;

import java.io.IOException;
import java.util.logging.Logger;

public class Main {
	private final static Logger LOGGER = Logger.getLogger(Main.class.getName());

	public static void main(String[] args) throws IOException {
		Stuff stuff = new Stuff();
	    stuff.parseVCF("C:/Users/soosh/Git/VCFParser/1984_S4.vcf");
		stuff.writeCSVFile();
	}
}
